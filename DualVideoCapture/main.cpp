// C++ includes
#include <iomanip>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <Windows.h> // GetModuleFileNameA, CreateProcessA

// visual studio version 2013 and above, set to 0
// visual studio version 2012 and below, set to 1 & add inttypes.h to .\packages\ffmpeg...\build\native\include\libavutil
#define OLD_VS 0

// ffmpeg livabdevice needs these
#define __STDC_CONSTANT_MACROS
#if OLD_VS == 0
	#include <inttypes.h>
#endif
#include <stdint.h>

// ffmpeg C-linkage
extern "C"
{
#if OLD_VS
	#include <libavutil/inttypes.h>
#endif
	#include <libavdevice/avdevice.h>
	#include <libavformat/avformat.h>
}

// default frames per second for video capture
#define FPS 15
#define MIN_FPS 1
#define MAX_FPS 60

// variables
std::vector<std::string> videoDevices;
std::vector<std::string> audioDevices;
int idVideoDevice0;
int idVideoDevice1;
int idAudioDevice;
int fps;
std::string directory;
std::string filename;
std::string logfile;

// functions
void printHeader();
int printMainMenu();
void enterToContinue();
void enterToExit();
void trim(std::string& str);
bool isValidOption(std::string& selection, int minOption, int maxOption, const std::string* exclude = NULL);
std::string getMyDirectory();
bool directoryExists(const std::string& dir);
bool fileExists(const std::string& fname);
int ffmpegGetDevices();
int selectVideoDevices();
int selectAudioDevice();
bool parseDirectory(std::string &dir);
void editDirectory();
void editFPS();
bool devicesOK();
int captureVideo();
void mergeSyncOutput();
void runProcess(char *cmd);
void convertAVItoMP4(const std::string& videofile, const std::string& outputfile);
int64_t getDuration(const std::string& filename);
void changeVideoSpeed(const std::string& inputfile, const std::string& outputfile, double pts);
void changeAudioSpeed(const std::string& inputfile, const std::string& outputfile, double atempo);
void mergeVideoAudio(const std::string& videofile, const std::string& audiofile, const std::string& outputfile);

// windows handle functions
struct handle_data
{
	unsigned long process_id;
	HWND best_handle;
};
BOOL is_main_window(HWND handle);
BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam);
HWND find_main_window(unsigned long process_id);


int main(int argc, char **argv)
{
	// initialize variables
	idVideoDevice0 = 0;
	idVideoDevice1 = 1;
	idAudioDevice = 0;
	fps = 15;
	directory = getMyDirectory();
	logfile = directory + "\output-log.txt";
	// get ffmpeg devices
	int ffmpegError = ffmpegGetDevices();
	if (ffmpegError != 0) {
		return ffmpegError;
	}
	// main menu
	while (true) {
		int opt = printMainMenu();
		switch (opt) {
		case 1:
		{
			int ffmpegErr = selectVideoDevices();
			if (ffmpegErr != 0) {
				return ffmpegErr;
			}
		}
		break;
		case 2:
		{
			int ffmpegErr = selectAudioDevice();
			if (ffmpegErr != 0) {
				return ffmpegErr;
			}
		}
		break;
		case 3:
			editFPS();
			break;
		case 4:
			editDirectory();
			break;
		case 5:
		{
			if (devicesOK) {
				int captureVideoError = captureVideo();
				if (captureVideoError != 0) {
					if (captureVideoError == 1) {
						break;
					}
					else {
						return captureVideoError;
					}
				}
			}
			else {
				std::cout << "ERROR: Check that there are 2 devices.\n";
				return -1;
			}
		}
		break;
		case 6:
			mergeSyncOutput();
			break;
		case 0:
		default:
			return 0;
			break;
		}
	}
	return 0;
}

void printHeader() {

	system("cls"); // clear console
	std::cout << "==================================================\n";
	std::cout << " Dual Video Capture\n";
	std::cout << "==================================================\n";
	std::cout << std::endl;
}

int printMainMenu() {

	printHeader();
	std::cout << "Video Device 1:    ";
	if (videoDevices.size() > idVideoDevice0) {
		std::cout << videoDevices[idVideoDevice0];
	}
	std::cout << "\n";
	std::cout << "Video Device 2:    ";
	if (videoDevices.size() > idVideoDevice1) {
		std::cout << videoDevices[idVideoDevice1];
	}
	std::cout << "\n";
	std::cout << "Audio Device:      ";
	if (audioDevices.size() > idAudioDevice) {
		std::cout << audioDevices[idAudioDevice];
	}
	std::cout << "\n";
	std::cout << "FPS:               " << fps << "\n";
	std::cout << "Output Directory:  " << directory << "\n\n";
	std::cout << "------------------------------\n";
	std::cout << " Menu Options\n";
	std::cout << "------------------------------\n";
	std::cout << " 1 - Select video devices\n";
	std::cout << " 2 - Select audio device\n";
	std::cout << " 3 - Edit FPS\n";
	std::cout << " 4 - Edit output directory\n";
	std::cout << " 5 - Begin video capture\n";
	std::cout << " 6 - Merge & sync output files\n";
	std::cout << " 0 - Exit program\n\n";
	std::string opt;
	while (std::getline(std::cin, opt) && !isValidOption(opt, 0, 6)) {}
	return atoi(opt.c_str());
}

void enterToContinue() {

	std::cout << "Press ENTER to continue..." << std::endl;
	std::cin.get();
}

void enterToExit() {

	std::cout << "Press ENTER to exit..." << std::endl;
	std::cin.get();
}

void trim(std::string& str) {

	if (str.length() != 0) {
		str.erase(0, str.find_first_not_of(" \t\n\r\f\v")); // left trim
	}
	if (str.length() != 0) {
		str.erase(str.find_last_not_of(" \t\n\r\f\v") + 1); // right trim
	}
}

bool isValidOption(std::string& selection, int minOption, int maxOption, const std::string* exclude) {

	trim(selection); // remove leading and trailing spaces from input
	try {
		std::string::size_type sz;
		int opt = std::stoi(selection, &sz);
		if (exclude == NULL) {
			return (opt >= minOption && opt <= maxOption);
		}
		else {
			int excl = std::stoi(*exclude, &sz);
			return (opt != excl && opt >= minOption && opt <= maxOption);
		}
	}
	catch (...) {
		return false;
	}
}

bool parseDirectory(std::string &dir) {

	trim(dir); // remove leading and trailing spaces
	try {
		// format default program directory
		if (dir.at(0) == '.') {
			dir.replace(0, 1, getMyDirectory());
		}
		// replace linux path separators
		std::string::size_type nf = 0;
		while ((nf = dir.find("/", nf)) != std::string::npos)
		{
			dir.replace(nf, 1, "\\");
			nf += 1;
		}
		// replace any instances of double path separators
		std::string::size_type nb = 2; // do not replace network directories
		while ((nb = dir.find("\\\\", nb)) != std::string::npos)
		{
			dir.replace(nb, 2, "\\");
			nb += 2;
		}
		// add path separator to the end
		if (dir.at(dir.size() - 1) != '\\') {
			dir += "\\";
		}
		return true;
	}
	catch (std::out_of_range& exception) {
		return false;
	}
}

std::string getMyDirectory() {

	char buffer[512];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string dir = std::string(buffer);
	std::string::size_type dirEnd;
	// VS 2012-: GetModuleFileNameA returns "...\project\debug\project.exe"
	// VS 2013+: GetModuleFileNameA returns "...\project\platform\debug\project.exe"
	int n = (OLD_VS) ? 2 : 3; // number of iterations for path separators
	// iterate through path separators to get to project folder
	for (int i = 0; i < n; ++i) {
		dirEnd = dir.find_last_of("\\/");
		dir = dir.substr(0, dirEnd);
	}
	return dir + "\\"; // add path separator to the end
}

bool directoryExists(const std::string& dir) {

	DWORD dwAttrib = GetFileAttributesA(dir.c_str());
	return (dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

bool fileExists(const std::string& fname) {

	DWORD dwAttrib = GetFileAttributesA(fname.c_str());
	return (dwAttrib != INVALID_FILE_ATTRIBUTES && !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

int ffmpegGetDevices() {

	// ***********************************************************
	// functions that call ffmpegGetDevices() should exit on error 
	// ***********************************************************
	printHeader();
	// write ffmpeg device output to pipe
	//   2>&1 - redirect(>) stderr(2) to stdout(1)
	//   "r"  - calling process can read spawned command's stdout using returned stream
	//   "t"  - open in text mode
	char psBuffer[2048];
	FILE *pPipe;
	if ((pPipe = _popen("ffmpeg.exe -list_devices true -f dshow -i dummy 2>&1", "rt")) == NULL) {
		std::cout << "ERROR: Could not initiate ffmpeg.\n";
		std::cout << std::endl;
		enterToExit();
		return -1;
	}
	// read pipe until end of file or an error occurs
	std::string dshowOutput;
	while (fgets(psBuffer, 2048, pPipe)) {
		dshowOutput += psBuffer;
	}
	// close pipe
	if (feof(pPipe)) {
		_pclose(pPipe); //printf("\nProcess returned %d\n", _pclose(pPipe));
	}
	else {
		std::cout << "ERROR: Failed to read from ffmpeg output.\n"; //printf("Error: Failed to read the pipe to the end.\n");
		std::cout << std::endl;
		enterToExit();
		return -2;
	}
	// initialize 2 video devices and 1 audio device
	bool videoFound = dshowOutput.find("Could not enumerate video devices") == std::string::npos;
	bool audioFound = dshowOutput.find("Could not enumerate audio only devices") == std::string::npos;
	if (!videoFound || !audioFound) {
		if (!videoFound) {
			std::cout << "ERROR: Video device(s) not connected!\n";
		}
		if (!audioFound) {
			std::cout << "ERROR: Audio device(s) not connected!\n";
		}
		std::cout << std::endl;
		enterToContinue();
		return 0;
	}
	// find start and end positions of device list
	std::size_t deviceStart = dshowOutput.find("[dshow @ ");
	std::size_t deviceEnd = dshowOutput.find("dummy: ");
	std::string deviceList = dshowOutput.substr(deviceStart, deviceEnd - deviceStart - 1);
	// find starting positions of video & audio devices
	std::size_t videoStart = dshowOutput.find("DirectShow video devices");
	std::size_t audioStart = dshowOutput.find("DirectShow audio devices");
	// add video devices
	videoDevices.clear(); // reset vector
	while ((videoStart = dshowOutput.find("]  \"", videoStart) + 4) < audioStart) {
		std::size_t deviceEnd = dshowOutput.find("\"", videoStart);
		videoDevices.push_back(dshowOutput.substr(videoStart, deviceEnd - videoStart));
		videoStart = deviceEnd;
	}
	if (videoDevices.size() == 0) {
		std::cout << "ERROR: No video devices found! Please connect 2 video devices.\n";
		std::cout << std::endl;
		enterToContinue();
		return 0;
	}
	else if (videoDevices.size() == 1)
	{
		std::cout << "ERROR: Only 1 video device found! Please connect video device 2.\n";
		std::cout << std::endl;
		enterToContinue();
		return 0;
	}
	// add audio devices
	audioDevices.clear(); // reset vector
	while ((audioStart = dshowOutput.find("]  \"", audioStart)) != std::string::npos) {
		audioStart += 4;
		std::size_t endFind = dshowOutput.find("\"", audioStart);
		audioDevices.push_back(dshowOutput.substr(audioStart, endFind - audioStart));
		audioStart = endFind;
	}
	if (audioDevices.size() == 0) {
		std::cout << "ERROR: No audio devices found! Please connect audio device.\n";
		idAudioDevice = 0;
		std::cout << std::endl;
		enterToContinue();
		return 0;
	}
	return 0;
}

int selectVideoDevices() {

	printHeader();
	// get current devices
	int ffmpegError = ffmpegGetDevices();
	if (ffmpegError != 0) {
		return ffmpegError;
	}
	// prompt for user input if devices were found
	if (videoDevices.size() >= 2) {
		std::cout << "Select video device number from list.\n\n";
		int i;
		for (i = 0; i < videoDevices.size(); ++i) {
			std::cout << (i + 1) << " - " << videoDevices[i] << "\n";
		}
		std::string opt0, opt1;
		std::cout << "\nVideo Device 1:  ";
		while (std::getline(std::cin, opt0) && !isValidOption(opt0, 1, i)) {}
		idVideoDevice0 = atoi(opt0.c_str()) - 1;
		std::cout << "Video Device 2:  ";
		while (std::getline(std::cin, opt1) && !isValidOption(opt1, 1, i, &opt0)) {}
		idVideoDevice1 = atoi(opt1.c_str()) - 1;
		std::cout << "\n";
	}
	return 0;
}

int selectAudioDevice()
{
	printHeader();
	// get current devices
	int ffmpegError = ffmpegGetDevices();
	if (ffmpegError != 0) {
		return ffmpegError;
	}
	// prompt for user input if device found
	if (audioDevices.size() >= 1) {
		std::cout << "Select audio device number from list.\n\n";
		int i;
		for (i = 0; i < audioDevices.size(); ++i) {
			std::cout << (i + 1) << " - " << audioDevices[i] << "\n";
		}
		std::string opt;
		std::cout << "\n";
		std::cout << "Audio Device:  ";
		while (std::getline(std::cin, opt) && !isValidOption(opt, 1, i)) {}
		idAudioDevice = atoi(opt.c_str()) - 1;
		std::cout << "\n";
	}
	return 0;
}

void editDirectory() {

	// prompt user for user input
	printHeader();
	std::cout << "Output Directory (. for program's directory):  ";
	std::string temp;
	std::getline(std::cin, temp);
	if (parseDirectory(temp)) {
		// attempt to make directory & check if it exists
		CreateDirectoryA(temp.c_str(), NULL);
		if (!directoryExists(temp)) {
			directory = getMyDirectory();
			std::cout << "\nERROR: Directory does not exist. Will use default program directory.\n";
			enterToContinue();
			return;
		}
		// set directory to user value
		directory = temp;
	}
	else {
		directory = getMyDirectory();
		std::cout << "\nERROR: Directory does not exist. Will use default program directory.\n";
		enterToContinue();
		return;
	}
}

void editFPS() {

	// prompt for user input
	printHeader();
	std::cout << "FPS (frames per second):  ";
	std::string temp;
	std::getline(std::cin, temp);
	trim(temp); // remove leading and trailing spaces from input
	// check fps range or set to default
	try {
		std::string::size_type sz;
		int itemp = std::stoi(temp, &sz);
		fps = (itemp < MIN_FPS) ? MIN_FPS : ((itemp > MAX_FPS) ? MAX_FPS : itemp);
	}
	catch (...) { // std::invalid_argument& e, std::out_of_range& e
		fps = FPS;
	}
}

bool devicesOK() {

	bool deviceMin = (videoDevices.size() >= 2) && (audioDevices.size() >= 1);
	bool deviceID = (videoDevices.size() > idVideoDevice0) && (videoDevices.size() > idVideoDevice1) && (audioDevices.size() > idAudioDevice);
	return (deviceMin && deviceID);
}

int captureVideo() {

	// set up video capture
	cv::VideoCapture vcap0;
	cv::VideoCapture vcap1;
	// open cameras
	bool open0 = vcap0.open(0);
	bool open1 = vcap1.open(1);
	// exit if cannot open cameras
	if (!open0 || !open1) {
		std::cout << "ERROR: Unable to open video devices! Please ensure they are both connected.\n";
		enterToContinue();
		return 1;
	}
	system("cls");
	printHeader();
	filename = ""; // reset filename
	std::cout << "Filename (do not include extension):  ";
	std::getline(std::cin, filename);
	trim(filename);
	if (filename.length() == 0) {
		return 1;
	}
	system("cls");
	printHeader();
	// double check if output directory exists
	CreateDirectoryA(directory.c_str(), NULL);
	if (!directoryExists(directory)) {
		std::cout << "ERROR: Output directory does not exist.\n";
		std::cout << "      " << directory << "\n";
		enterToContinue();
		return 1;
	}
	std::string videoData0 = directory + filename + "-video-0.avi";
	std::string videoData1 = directory + filename + "-video-1.avi";
	std::string audioData = directory + filename + "-audio.mp4";
	std::cout << "Output files:\n";
	std::cout << videoData0 << "\n" << videoData1 << "\n" << audioData << "\n";
	std::cout << "\n";
	std::cout << "Press ENTER when you are ready to begin recording.\n";
	std::cout << "Press ESC in Video Output window to stop/exit.\n";
	std::cout << "\n";
	// where to store temporary frames
	cv::Mat frame0;
	cv::Mat frame1;
	// show live video capture first without recording
	while (true)
	{
		vcap0 >> frame0;
		vcap1 >> frame1;
		// check for end of video streams
		if (frame0.empty()) {
			std::cout << "ERROR: Unable to get frames from Camera 1!\n";
			enterToExit();
			return -6;
		}
		if (frame1.empty()) {
			std::cout << "ERROR: Unable to get frames from Camera 2!\n";
			enterToExit();
			return -6;
		}
		// show frames
		imshow("Video Output 1", frame0);
		imshow("Video Output 2", frame1);
		// user input
		char vKeyInput = cv::waitKey(10);
		if (vKeyInput == 27) { // ESC
			cv::destroyAllWindows(); // close video output
			return 1; // exit back to main menu
		}
		if (vKeyInput == 13) { // ENTER
			break;
		}
	}
	cv::destroyAllWindows();
	// set up video 0
	int width0 = vcap0.get(CV_CAP_PROP_FRAME_WIDTH);
	int height0 = vcap0.get(CV_CAP_PROP_FRAME_HEIGHT);
	cv::VideoWriter output0(videoData0, CV_FOURCC('M', 'J', 'P', 'G'), FPS, cv::Size(width0, height0), true);
	// set up video 1
	int width1 = vcap1.get(CV_CAP_PROP_FRAME_WIDTH);
	int height1 = vcap1.get(CV_CAP_PROP_FRAME_HEIGHT);
	cv::VideoWriter output1(videoData1, CV_FOURCC('M', 'J', 'P', 'G'), FPS, cv::Size(width1, height1), true);
	// set up audio capture
	std::string strAudio = "ffmpeg.exe -f dshow -i audio=\"" + audioDevices[idAudioDevice] + "\" -y \"" + audioData + "\"";
	char *cmdAudio = new char[strAudio.size() + 1];
	cmdAudio[strAudio.size() - 1] = '\0';
	strncpy_s(cmdAudio, strAudio.size() + 1, strAudio.c_str(), strAudio.size());
	// create process to capture audio
	STARTUPINFOA siAudio;
	PROCESS_INFORMATION piAudio;
	ZeroMemory(&siAudio, sizeof(siAudio));
	siAudio.cb = sizeof(siAudio);
	ZeroMemory(&piAudio, sizeof(piAudio));
	// do not use CREATE_NO_WINDOW - process is terminated using window handle
	if (!CreateProcessA(NULL, cmdAudio, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE | CREATE_NEW_PROCESS_GROUP | CREATE_UNICODE_ENVIRONMENT, NULL, NULL, &siAudio, &piAudio)) {
		std::cout << "ERROR: CreateProcess for audio capture failed (" << GetLastError << ").\n\n";
		enterToExit();
		return -5;
	}
	// record video frames
	for (;;) {
		vcap0 >> frame0;
		vcap1 >> frame1;
		// check for end of video streams
		if (frame0.empty()) {
			std::cout << "ERROR: Unable to get frames from Camera 1!\n";
			enterToExit();
			return -6;
		}
		if (frame1.empty()) {
			std::cout << "ERROR: Unable to get frames from Camera 2!\n";
			enterToExit();
			return -6;
		}
		// write to frames, show one video stream
		output0.write(frame0);
		output1.write(frame1);
		imshow("Video Output 1", frame0);
		imshow("Video Output 2", frame1);
		// press ESC inside video output window to stop capture
		if (cv::waitKey(10) == 27) {
			break;
		}
	}
	// close opencv windows
	cv::destroyAllWindows();
	// close audio capture process
	CloseHandle(piAudio.hProcess);
	CloseHandle(piAudio.hThread);
	HWND windowHandle = find_main_window(piAudio.dwProcessId);
	PostMessage(windowHandle, WM_CLOSE, 0, 0);
	// write to output log
	std::ofstream outputLog;
	outputLog.open(logfile, std::ios_base::app);
	outputLog << directory << filename << "\n";
	return 0;
}

void mergeSyncOutput() {

	// prompt user
	printHeader();
	std::cout << "Select files to sync & merge:\n\n";
	std::ifstream ilog;
	ilog.open(logfile, std::ifstream::in);
	std::string line;
	std::vector<std::string> allLines;
	int i = 0;
	while (std::getline(ilog, line)) {
		std::string vid0 = line + "-video-0.avi";
		std::string vid1 = line + "-video-1.avi";
		std::string aud = line + "-audio.mp4";
		bool exists = fileExists(vid0) && fileExists(vid1) && fileExists(aud);
		if (exists) {
			++i;
			std::cout << " " << i << " - " << line << "-video-0.avi\n";
			std::cout << "     " << line << "-video-1.avi\n";
			std::cout << "     " << line << "-audio.mp4\n";
			allLines.push_back(line);
		}
	}
	ilog.close();
	int nline;
	// prompt user for directory and tag if no valid files found in log
	if (allLines.size() == 0) {
		std::ofstream outputLog;
		outputLog.open(logfile, std::ofstream::out | std::ofstream::trunc);
		outputLog.close();
		// prompt user
		printHeader();
		std::cout << "Enter directory of output files:  ";
		std::string dir;
		std::getline(std::cin, dir);
		if (parseDirectory(dir)) {
			// get file tag
			std::cout << "\nExample output files:  C:\\data\\sample-video-0.avi\n";
			std::cout << "                       C:\\data\\sample-video-1.avi\n";
			std::cout << "                       C:\\data\\sample-audio.mp4\n";
			std::cout << "File tag:              sample\n\n";
			std::cout << "Enter file tag:  ";
			std::string tag;
			std::getline(std::cin, tag);
			std::string vid0 = dir + tag + "-video-0.avi";
			std::string vid1 = dir + tag + "-video-1.avi";
			std::string aud = dir + tag + "-audio.mp4";
			bool exists = fileExists(vid0) && fileExists(vid1) && fileExists(aud);
			if (exists) {
				++i;
				std::cout << "\n";
				allLines.push_back(dir + tag);
				nline = 0;
			}
			else {
				std::cout << "\nERROR: Cannot find the necessary output files\n";
				std::cout << "       " + vid0  + "\n";
				std::cout << "       " + vid1 + "\n";
				std::cout << "       " + aud + "\n\n";
				enterToContinue();
				return;
			}
		}
		else {
			std::cout << "\nERROR: Directory does not exist.\n";
			enterToContinue();
			return;
		}
	}
	else {
		std::cout << " 0 - Cancel and return to main menu\n\n";
		std::string opt;
		while (std::getline(std::cin, opt) && !isValidOption(opt, 0, i)) {}
		nline = atoi(opt.c_str()) - 1;
		// exit option 0
		if (nline < 0) {
			return;
		}
	}
	if (fileExists(allLines[nline] + "-video-0.avi")) {
		std::cout << "Converting video-0 from avi to mp4...\n";
		convertAVItoMP4(allLines[nline] + "-video-0.avi", allLines[nline] + "-video-0.mp4");
	}
	if (fileExists(allLines[nline] + "-video-1.avi")) {
		std::cout << "Converting video-1 from avi to mp4...\n";
		convertAVItoMP4(allLines[nline] + "-video-1.avi", allLines[nline] + "-video-1.mp4");
	}
	// slow down video to sync with audio
	if (fileExists(allLines[nline] + "-audio.mp4") && fileExists(allLines[nline] + "-video-0.mp4") && fileExists(allLines[nline] + "-video-1.mp4")) {
		int64_t videoTime0 = getDuration(allLines[nline] + "-video-0.mp4");
		int64_t videoTime1 = getDuration(allLines[nline] + "-video-1.mp4");
		int64_t audioTime = getDuration(allLines[nline] + "-audio.mp4");
		double pts0 = (double)audioTime / videoTime0;
		double pts1 = (double)audioTime / videoTime1;
		std::cout << "Syncing video & audio files...\n";
		changeVideoSpeed(allLines[nline] + "-video-0.mp4", allLines[nline] + "-video-slow-0.mp4", pts0);
		changeVideoSpeed(allLines[nline] + "-video-1.mp4", allLines[nline] + "-video-slow-1.mp4", pts1);
	}
	if (fileExists(allLines[nline] + "-video-slow-0.mp4") && fileExists(allLines[nline] + "-video-slow-1.mp4")) {
		// merge video & audio
		std::cout << "\n";
		std::cout << "Merging audio & video files...\n";
		mergeVideoAudio(allLines[nline] + "-video-slow-0.mp4", allLines[nline] + "-audio.mp4", allLines[nline] + "-0.mp4");
		mergeVideoAudio(allLines[nline] + "-video-slow-1.mp4", allLines[nline] + "-audio.mp4", allLines[nline] + "-1.mp4");
	}
	// overwrite log file to remove converted and invalid paths
	std::ofstream olog("temp-log.txt");
	ilog.open(logfile, std::ifstream::in);
	while (std::getline(ilog, line)) {
		if (line != allLines[nline]) {
			olog << line << "\n";																																																							//
		}
	}
	ilog.close();
	olog.close();
	remove(logfile.c_str());
	rename("temp-log.txt", logfile.c_str());
}

void runProcess(char *cmd)
{
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	if (!CreateProcessA(NULL, cmd, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW/*CREATE_NEW_CONSOLE*/ | CREATE_NEW_PROCESS_GROUP | CREATE_UNICODE_ENVIRONMENT, NULL, NULL, &si, &pi)) {
		std::cout << "$ " << cmd << "\n\n";
		std::cout << "ERROR: CreateProcess() failed with error number (" << GetLastError << ").\n";
		enterToContinue();
	}
	else {
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
}

void convertAVItoMP4(const std::string& videofile, const std::string& outputfile) {

	/*bool aviFound = true; // (videofile.length() > 4) ? (videofile.substr(videofile.size - 4, 4) == ".avi") : false;
	bool mp4Found = true; // (outputfile.length() > 4) ? (outputfile.substr(outputfile.size - 4, 4) == ".mp4") : false;
	if (aviFound && mp4Found) {*/
	if (fileExists(videofile)) {
		// $ ffmpeg -i input.avi -c:v libx264 -crf 19 -preset slow -c:a libfdk_aac -b:a 192k -ac 2 output.mp4
		std::string str = "ffmpeg.exe -i \"" + videofile + "\" -c:v libx264 -crf 19 -preset slow -c:a libfdk_aac -b:a 192k -ac 2 \"" + outputfile + "\"";
		char *cmd = new char[str.size() + 1];
		cmd[str.size() - 1] = '\0';
		strncpy_s(cmd, str.size() + 1, str.c_str(), str.size());
		runProcess(cmd);
	}
	else {
		std::cout << "ERROR: convertAVItoMP4() failed. Could not find video file:\n" << videofile << "\n";
		enterToContinue();
	}
}

int64_t getDuration(const std::string& filename) {

	// $ ffprobe -i input.mp4 -show_entries format=duration -v quiet -of csv="p=0"
	if (fileExists(filename)) {
		av_register_all();
		AVFormatContext* pFormatCtx = avformat_alloc_context();
		avformat_open_input(&pFormatCtx, filename.c_str(), NULL, NULL);
		avformat_find_stream_info(pFormatCtx, NULL);
		int64_t duration = pFormatCtx->duration;
		avformat_close_input(&pFormatCtx);
		avformat_free_context(pFormatCtx);
		return duration;
	}
	else {
		std::cout << "ERROR: getDuration() failed. Could not find file:\n" << filename << "\n";
		enterToContinue();
		return 0;
	}
}

void changeVideoSpeed(const std::string& videofile, const std::string& outputfile, double pts) {

	if (fileExists(videofile)) {
		// set precision for pts
		std::ostringstream rate;
		rate << std::setprecision(5) << pts;
		// $ ffmpeg -i input.mp4 -filter:v "setpts=(1.0)*PTS" -an output.mp4
		// pts > 1 slows down video
		// pts < 1 speeds up video
		std::string str = "ffmpeg.exe -i " + videofile + " -filter:v \"setpts=(" + rate.str() + ")*PTS\" -an \"" + outputfile + "\"";
		char *cmd = new char[str.size() + 1];
		cmd[str.size() - 1] = '\0';
		strncpy_s(cmd, str.size() + 1, str.c_str(), str.size());
		runProcess(cmd);
	}
	else {
		std::cout << "ERROR: changeVideoSpeed() failed. Could not find video file:\n" << videofile << "\n";
		enterToContinue();
	}
}

void changeAudioSpeed(const std::string& audiofile, const std::string& outputfile, double atempo) {

	if (fileExists(audiofile)) {
		// set precision for atmepo
		double a = (atempo < 0.5) ? 0.5 : ((atempo > 2.0) ? 2.0 : atempo);
		std::ostringstream rate;
		rate << std::setprecision(5) << a;
		// $ ffmpeg -i input.mp4 -filter:a "atempo=1.0" -vn output.mp4
		// atempo < 1 slows down video (minimum 0.5, half speed)
		// atempo > 1 speeds up video (maximum 2.0, 2x speed)
		std::string str = "ffmpeg.exe -i " + audiofile + " -filter:a \"atempo=(" + rate.str() + "\" -vn \"" + outputfile + "\"";
		char *cmd = new char[str.size() + 1];
		cmd[str.size() - 1] = '\0';
		strncpy_s(cmd, str.size() + 1, str.c_str(), str.size());
		runProcess(cmd);
	}
	else {
		std::cout << "ERROR: changeAudioSpeed() failed. Could not find audio file:\n" << audiofile << "\n";
		enterToContinue();
	}
}

void mergeVideoAudio(const std::string& videofile, const std::string& audiofile, const std::string& outputfile) {

	if (fileExists(videofile) && fileExists(audiofile)) {
		// $ ffmpeg -i video.mp4 -i audio.m4a -c:v copy -c:a copy out.mp4
		std::string str = "ffmpeg.exe -i \"" + videofile + "\" -i \"" + audiofile + "\" -c:v copy -c:a copy \"" + outputfile + "\"";
		char *cmd = new char[str.size() + 1];
		cmd[str.size() - 1] = '\0';
		strncpy_s(cmd, str.size() + 1, str.c_str(), str.size());
		runProcess(cmd);
	}
	else {
		std::cout << "ERROR: mergeVideoAudio() failed. Could not find file(s):\n" << videofile << "\n" << audiofile << "\n";
		enterToContinue();
	}
}

// windows handle functions

BOOL is_main_window(HWND handle)
{
	return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
}

BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam)
{
	handle_data& data = *(handle_data*)lParam;
	unsigned long process_id = 0;
	GetWindowThreadProcessId(handle, &process_id);
	if (data.process_id != process_id || !is_main_window(handle)) {
		return TRUE;
	}
	data.best_handle = handle;
	return FALSE;
}

HWND find_main_window(unsigned long process_id)
{
	handle_data data;
	data.process_id = process_id;
	data.best_handle = 0;
	EnumWindows(enum_windows_callback, (LPARAM)&data);
	return data.best_handle;
}
