------------------
Dual Video Capture
------------------
------------------

This program allows for the capture of 2 simultaneous video streams, using OpenCV for video capture and FFMPEG for audio capture.

This project was created to work with Visual Studio 2013 and 2015 but I added some modifications in the main.cpp since I also needed it to work in 2012.  
In order to use this with 2012:  
- Create a new project and add OpenCV and FFMPEG via the NuGet Package Manager  
- Add <inttypes.h> header file inside the NuGet FFMPEG include directory ../libavutil/  
- Copy the main.cpp file over to the 2012 project  
- Change the OLD_VS macro value to 1: #define OLD_VS 1  

Menu Options
------------
1. Select video devices:  
	Populates list of connected available video devices and prompts user to select 2 devices for video capture.
2. Select audio devices:  
	Populates list of connected available audio devices and prompts user to select 1 device for audio capture.
3. Edit FPS (optional):  
	Allows user to change frames per second for video capture (acceptable range from 1 to 60, defaults to 15).
4. Edit output directory (optional):  
	Allows user to specify where the output files will be created. Defaults to program's directory.
5. Begin video capture:  
	Prompts user for the file name for output files. The following 3 files will be created:  
		<output_directory>\<file_name>-video-0.avi  
		<output_directory>\<file_name>-video-1.avi  
		<output_directory>\<file_name>-audio.avi  
	An entry will also be added to the output-log.txt file for merging/syncing files in the next step.  
	Press ENTER key to view the video capture device screens and adjust camera positions if necessary.  
	Press ENTER key again to begin recording and ESC key to stop recording (focus must be on either one of the video capture screens to start/stop).  
	Once you begin recording, you will see a separate FFMPEG console window which will be capturing the audio in the background.  
6. Merge & sync output files:  
	Uses information from created videos in the output-log.txt file to create the final videos.  
	If the log does not contain valid files, it prompts the user for a directory and file name tag (this would be the <file_name> in <file_name>-video-0.avi).  
	There are several steps involved in this option and may take some time depending on length and size of the videos.  
	Merge & sync was made into a separate option to allow for recording dual videos in succession without having to wait through the steps.  
	First, the <file_name>-video-0.avi and <file_name>-video-1.avi files are converted to mp4.  
	Next, the newly created mp4 video files are adjusted to sync with the <file_name>-audio.mp4 file.  
	Finally, the time-adjusted video files are merged with the audio file to create 2 final videos:  
		<output_directory>\<file_name>-0.mp4  
		<output_directory>\<file_name>-1.mp4  